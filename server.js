var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosmlab="https://api.mlab.com/api/1/databases/dbbanca2/collections/movimientos?apiKey=HpRzVsg4Fc_mb2_Rshem6s6jwQye4IMm";

var clienteMLab=requestjson.createClient(urlmovimientosmlab);

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/movimientos", function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
});

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
  //res.send("Hello World!!!");
});

app.get("/clientes/:idcliente", function(req, res){
  //res.sendFile(path.join(__dirname, 'index.html'));
  //res.send("Hello World!!!");
  res.send("Se procesa el cliente con ID: " + req.params.idcliente);
});

/*
app.post("/movimientos/add", function(req, res){
  var data = {"idcliente": 8888,"nombre": "Juan","apellido": "Json","listado": [{"fecha": "21/08/2018","importe": 43.95,"categoria": "A"}, { "fecha": "21/08/2018", "importe": 22.95, "categoria": "B"}]}
    console.log(data);
    clienteMLab.post('', data, function(err, resM, body){
      if(err){
        console.log(body);
      }else{
        res.send(body);
      }
    })
});
*/

app.post("/movimientos", function(req, res){
    clienteMLab.post('', req.body, function(err, resM, body){
        if(err){
            console.log(body);
        }else{
            res.send(body);
        }
    })
});

app.post("/", function(req, res){
  res.send("Hemos recibido su petición!!! En proceso...");
});

app.put("/", function(req, res){
  //res.sendfile(path.join(__dirname, 'index.html'));
  res.send("Recibimos su modificación...");
});

app.delete("/", function(req, res){
  res.send("Estamos procesando su borrado...");
});
